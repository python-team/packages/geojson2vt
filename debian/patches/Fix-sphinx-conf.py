Description: Fix Sphinx conf.py
--- geojson2vt-1.0.1+debian.orig/docs/conf.py
+++ geojson2vt-1.0.1+debian/docs/conf.py
@@ -20,6 +20,7 @@
 project = 'geojson2vt'
 copyright = '2020, Samuel Kurath'
 author = 'Samuel Kurath'
+master_doc = 'index'
 
 # The full version, including alpha/beta/rc tags
 release = '1.0.1'
